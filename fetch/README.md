fetch
=====

The fetch program connects over SFTP to fetch the rolling backups from targets listed in `config.json`.

The idea is to keep the backups in multiple places in case of failure.

This program is meant to run on a schedule (e.g. via cron or systemd).

Each sub-dir contains an implementation of fetch.

Behaviour
---------

1. get a list of files matching the pattern in the remote dir
2. sort them alphabetically
	* (if backups are named with timestamps the way that the backup program does then this is the same as sorting by date from oldest to newest)
3. download the bottom (newest) `maxnumber` of files
4. delete any extra files in `localdir`-- that is, sort by name then delete the "oldest" to reach `maxnumber`

Usage
-----

<pre>
	--config, -c (FILENAME) = a .json file containing the list of targets
	--verbose, -v = print output during execution
</pre>

Config file
-----------
`config.json` should look like so:
<pre>
{
	"targets": [
		{
			"name": "backups of fubar",
			"host": "foobar.baz",
			"username": "fubur",
			"password": "asdfasdf",
			"pattern": "*fubar.tar.gz",
			"localdir": "./backups",
			"remotedir": "/home/fubur/backups",
			"maxnumber": 5
		}
	]
}
</pre>