#!/usr/bin/ruby

require 'optparse'
require 'json'
require 'net/sftp'

class Fetcher
	public
		def initialize opts
			@@opts = opts
		end
		
		def run
			@@opts[:targets].each do |target|
				log "Fetching '#{target['name']}'..."
				fetch target
				numDeleted = trim "#{target['localdir']}/#{target['pattern']}", target['maxnumber']
				log "#{target['name']} : Deleted #{numDeleted} files to maintain a maximum of #{target['maxnumber']} backups."
			end
		end
	
	protected
		#
		#	target is expected to be a hash like so:
		#	{
		#		"targets": [
		#			{
		#				"name": "backups of fubar",
		#				"host": "foobar.baz",
		#				"username": "fubur",
		#				"password": "asdfasdf",
		#				"pattern": "*fubar.tar.gz",
		#				"localdir": "./backups",
		#				"remotedir": "/home/fubur/backups",
		#				"maxnumber": 5
		#			}
		#		]
		#	}
		#
		def fetch target
			Net::SFTP.start(target['host'], target['username'], :password => target['password']) do |sftp|
				log 'connected.'
				files = sftp.dir.glob(target['remotedir'], target['pattern'])
				downloadList = files.sort_by!{|f| f.name}.drop(files.length - target['maxnumber'])
				
				downloadList.each do |f|
					log "downloading '#{f.name}'..."
					remotePath = "#{target['remotedir']}/#{f.name}"
					localPath = "#{target['localdir']}/#{f.name}"
					sftp.download! remotePath, localPath
				end
			end
		end
		
		# delete the oldest if we're over
		def trim path, maxn
			# note that because of the way backups are named alphabetical order is the same as oldest first
			backups = Dir[path].sort
			count = 0
			if backups.length > maxn
				backups[0, backups.length - maxn].each do |b|
					File.delete b
					count += 1
				end
			end
			return count
		end
		
		# print msg but only if in verbose mode
		def log msg
			puts msg if @@opts[:verbose] == true
		end
end

#
# parse the args and load up the config
#
if __FILE__ == $0
	opts = {
		:config => './config.json',
		:verbose => false
	}
	
	p = OptionParser.new do |o|
		o.on('-c', '--config CONFIGFILE', 'File containing JSON configuration.') { |c| opts[:config] = c }
		o.on('-v', '--verbose', 'Print output during execution.') { |v| opts[:verbose] = true }
	end
	
	begin p.parse!
	rescue OptionParser::InvalidOption => e
		puts 'InvalidOption'
		puts p.help
	end
	
	if !File.exists? opts[:config]
		puts "Could not find '#{opts[:config]}'"
		puts p.help
		exit 1
	end
	
	begin config = File.read opts[:config]
	rescue
		puts "Could not read '#{opts[:config]}'"
		exit 1
	end
	
	begin cfg = JSON.parse config
	rescue
		puts "Failed to JSON.parse '#{opts[:config]}'"
		exit 1
	end
	
	opts[:targets] = cfg['targets']
	Fetcher.new(opts).run
end