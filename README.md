rolling-backup
==============

Frequently developers need to generate and keep a rolling backup of something. They don't need, or it's politically impossible to obtain, a full-scale "enterprise" class backup solution when a quick little script could do the job.

This project consists of two parts:

1. backup -- does the backing up by creating tarballs.
2. fetch -- fetches backups over SFTP from a target machine.

Both are meant to be run via a task scheduler like [cron](https://en.wikipedia.org/wiki/Cron) or [systemd timers](https://wiki.archlinux.org/index.php/Systemd/Timers).