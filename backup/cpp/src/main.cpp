#include <iostream>
#include <string>
using namespace std;

#include <boost\program_options.hpp>
namespace po = boost::program_options;

#include "RollingBackup.hpp"

int main (int argc, char ** argv) {
	auto backupdir = string { "" };
	auto target = string { "" };
	auto maxNum = short {};
	auto argerr = false;
	auto verbose = false;

	auto opts = po::options_description { "Options" };
	opts.add_options ()
		("help,h", "list options")
		("destination,d", po::value<string> (&backupdir)->required (), "path to an existing dir where the rolling backups are kept")
		("target,t", po::value<string> (&target)->required (), "path to a dir or file to compress to a tarball and store in backupdir")
		("maxn,m", po::value<short> (&maxNum)->required (), "maximum number of target to keep around (oldest will be deleted)")
		("verbose,V", po::value<bool> (&verbose), "list files that are being backed up.");

	auto optvars = po::variables_map {};
	try {
		po::store (po::parse_command_line (argc, argv, opts), optvars);
		po::notify (optvars);
	} catch (po::unknown_option e) {
		cout << "Unknown option \"" << e.get_option_name () << "\"" << endl;
		argerr = true;
	} catch (po::required_option e) {
		cout << "Option required: " << e.get_option_name () << "\"" << endl;
		argerr = true;
	}
	if (!optvars.size() || optvars.count ("help") || argerr) {
		cout << opts << endl;
		return 0;
	}

	try {

		RollingBackup::backup (backupdir, target, maxNum, verbose);

	} catch (std::exception e) {
		cout << e.what () << endl;
		return 1;
	}

	return 0;
}
