#include <iostream>
#include <string>
#include <ctime>
#include <cstdio>
#include <algorithm>
#include <vector>

#include <boost/filesystem.hpp>
#include <boost/regex.hpp>

// if LIBARCHIVE_STATIC is not defined then libarchive prepends "__imp__" in front of all function names
// so you'll get linker errors saying it can't find, for example, __imp__archive_write_new()
#ifndef LIBARCHIVE_STATIC
	#define LIBARCHIVE_STATIC
#endif
// libarchive: https://github.com/libarchive/libarchive/
// as of 2015-08-19 rollingbackup uses libarchive v2.4.12
#include <archive.h>
#include <archive_entry.h>

#include "RollingBackup.hpp"

using namespace std;
using namespace RollingBackup;
namespace bf = boost::filesystem;

//
//	throws an exception if dest doesn't exist or isn't a dir or if target doesn't exist
//
void validate (const string & dest, const string & target) {
	auto err = string ("RollingBackup::validate() -- ");
	if (!bf::exists (dest)) {
		err += "backup dir doesn't exist: \"";
		err += dest;
		err += "\"";
		throw RollingBackupException { err };
	}

	if (!bf::is_directory (dest)) {
		err += "backup dir must be a directory: \"";
		err += dest;
		err += "\"";
		throw RollingBackupException { err };
	}

	if (!bf::exists (target)) {
		err += "backup target doesn't exist: \"";
		err += target;
		err += "\"";
		throw RollingBackupException { err };
	}
}

//
//	returns a complete list of files to back up
//
//	if targetPath is a single file then return that
//	if targetPath is a dir then return a recursive list of everything in it
//
vector<string> getTargetList (const string & targetPath) {
	auto rtrn = vector<string> ();

	if (bf::is_regular_file (targetPath)) {
		rtrn.push_back (targetPath);

	} else if (bf::is_directory (targetPath)) {

		// note: we could use bf::recursive_directory_iterator ...
		auto dirIterator = bf::directory_iterator (targetPath);
		for (auto f : dirIterator) {
			auto subdirlist = getTargetList (f.path ().generic_string ());
			rtrn.insert (rtrn.end (), subdirlist.begin (), subdirlist.end ());
		}
	}

	return rtrn;
}

//
//	uses libarchive (https://github.com/libarchive/libarchive)
//	as of 2015-08-19 this function uses libarchive v2.4.12
//
void writeArchive (const string & outname, const vector<string> & targetFiles) {
	struct archive * arc;
	struct archive_entry * entry;
	struct stat st;
	auto failedFile = string { "" };

	arc = archive_write_new ();
		archive_write_set_compression_gzip (arc);
		archive_write_set_format_pax_restricted (arc);
	if (archive_write_open_filename (arc, outname.c_str ()) != ARCHIVE_OK) {
		cout << "Failed to open archive for write: " << archive_error_string (arc) << endl;
		archive_write_close (arc);
		archive_write_finish (arc);
		return;
	}
	entry = archive_entry_new ();

	for (auto f : targetFiles) {
		auto fname = f.c_str ();
		stat (fname, &st);
			archive_entry_copy_stat (entry, &st);
			archive_entry_copy_pathname (entry, fname);
			archive_entry_set_size (entry, st.st_size);
			archive_entry_set_filetype (entry, AE_IFREG);
			archive_entry_set_perm (entry, 644); // set to rw-r--r--

		if (archive_write_header (arc, entry) != ARCHIVE_OK) {
			cout << "Failed to write data to archive: " << archive_error_string (arc) << endl;
			archive_entry_free (entry);
			archive_write_close (arc);
			archive_write_finish (arc);
			return;
		}

		auto fil = fopen (f.c_str (), "rb");
		if (!fil) {
			archive_entry_free (entry);
			archive_write_close (arc);
			archive_write_finish (arc);
			throw RollingBackupException { (f + " -- Failed to open file for reading") };
		}
		
		char buff [1024];
		auto numBytesRead = size_t { 1 };
		while (numBytesRead > 0) {
			numBytesRead = fread (buff, sizeof(char), 1024, fil);
			assert (numBytesRead <= 1024);
			archive_write_data (arc, buff, numBytesRead);
		}
		fclose (fil);

		// wait, is this also wiping the file? do I need to flush before clear?
		archive_entry_clear (entry);
	}
	archive_entry_free (entry);
	archive_write_close (arc);
	archive_write_finish (arc);
}

//
// return the full path + timestamp + target + ".tar.gz"
// i.e. "/foo/bar/baz/2015-08-12_7-15_someTarget.tar.gz"
//
string getDestinationArchiveName (const string & backupDestination, const string & backupTarget) {
	auto rawtime = time_t {};
	time (&rawtime);
	auto timeinfo = localtime (&rawtime);
	char timestamp [80];
	strftime (timestamp, 80, "%Y-%m-%d_%I%M%S", timeinfo);

	auto rtrn = string { backupDestination + (char)bf::path::preferred_separator };
	rtrn += timestamp;
	rtrn += "_";
	rtrn += bf::path (backupTarget).filename ().string ();
	rtrn += ".tar.gz";
	return rtrn;
}

//
// return just the filename with underscores and no spaces no extensions
// "C:\foo bar - baz.biddle" => "foo_bar_-_baz"
//
string getCleanFName (const string & path) {
	auto name = bf::path { path }.stem ().string ();
	std::replace (name.begin (), name.end (), ' ', '_');
	return name;
}

//
//	if there's more than maxNum backups in dir then delete the oldest 'till we reach maxNum
//	returns the number of files deleted
//	note: We're using regex to strictly filter the list of files that are considered to be backups.
//		So if you want to spare a file then name is so that the regex doesn't match it.
//		The regex is looking for "0000-00-00_000000_BACKUP_TARGET.tar.gz"
//		So, for example, "spare-me_BACKUP_TARGET.tar.gz" will be spared from deletion.
//
int deleteOlderThanN (const string & dir, const string & fname, const unsigned short & maxNum) {
	auto backupsList = vector<string> {};

	auto patternStr = string { ".*\\d{4}-\\d{2}-\\d{2}_\\d{6}_" };
	patternStr += fname + "\\.tar\\.gz";
	// will throw a boost::regex_error if the regex isn't syntactically valid etc.
	auto filter = boost::regex { patternStr };

	for (auto f : bf::directory_iterator (dir)) {
		if (bf::is_regular_file (f)) {
			auto fpath = f.path ().string ();
			if (boost::regex_match (fpath, filter)) {
				backupsList.push_back (fpath);
			}
		}
	}
	
	auto nToDelete = static_cast<int> (backupsList.size () - maxNum);
	if (nToDelete <= 0) {
		return 0;
	}

	// sort by time (alphabetical is the same as oldest first because of the way they're named)
	std::sort (backupsList.begin (), backupsList.end ());
	// and kill off the oldest 'till we reach maxNum
	auto ndeleted = 0;
	while ((ndeleted < nToDelete) && (ndeleted < backupsList.size ())) {
		bf::remove (backupsList [ndeleted]);
		ndeleted++;
	}
	return ndeleted;
}

//
//	call this to do the work (this is the "main" function)
//
bool RollingBackup::backup (const string & backupDestination, const string & backupTarget, const unsigned short & maxNum, const bool & verbose) {
	// throws exceptions if these paths don't exist
	validate (backupDestination, backupTarget);
	auto cleanTargetName = getCleanFName (backupTarget);
	auto targetFilePaths = getTargetList (backupTarget);
	auto destFname = getDestinationArchiveName (backupDestination, cleanTargetName);
	
	if (verbose) {
		cout << "Backup basename: \"" << cleanTargetName << "\"" << endl;
		for (auto f : targetFilePaths) {
			cout << "Backing up \"" << f << "\"" << endl;
		}
	}
	
	// put everything into a tarball
	writeArchive (destFname, targetFilePaths);
	if (verbose) {
		cout << "Archive written to \"" << destFname << "\"" << endl;
	}

	// check timestamps of backups and delete the oldest if N is bigger than maxn
	auto ndeleted = deleteOlderThanN (backupDestination, cleanTargetName, maxNum);
	if (verbose) {
		cout << "Deleted " << ndeleted << " files to keep number of backups less than or equal to " << maxNum << "." << endl;
	}

	return true;
}
