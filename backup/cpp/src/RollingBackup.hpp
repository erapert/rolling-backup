#ifndef ROLLING_BACKUP_H
#define ROLLING_BACKUP_H

#include <string>

namespace RollingBackup {
	class RollingBackupException : public std::exception {
		private:
			std::string msg;
		public:
			RollingBackupException (const std::string & message) : msg (message) {}
			~RollingBackupException () {};
			const char * what () { return msg.c_str (); }
	};

	bool backup (const std::string & backupDestination, const std::string & backupTarget, const unsigned short & maxNum, const bool & verbose);
};

#endif