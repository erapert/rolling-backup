Compiling
---------

###Requirements:

build tools:

* [cmake](http://www.cmake.org/) Used for generating project files on Windows, Linux, and Mac.
* a C++11 compliant compiler (g++ v4.8+ or MSVC 2013+ ... I haven't built it on clang yet)

on windows:

* [gnuwin32](http://gnuwin32.sourceforge.net/packages.html) with libarchive and zlib installed
* [mingw](http://www.mingw.org/) to use the g++ compiler and make.

libraries:

* [boost](http://www.boost.org/) Used for arg parsing on the command line.
* [libarchive](http://www.libarchive.org) v2.4.12 which requires zlib to work with gzip (.tar.gz) (gnuwin32 provides this as of 2015-08-19)
* [zlib](http://www.zlib.net/) v1.2.3 (or whatever works with libarchive) (gnuwin32 provides this as of 2015-08-19)

###Building
1. Make sure you have the shared libarchive and the boost libs on your PATH where the compiler (and executable) can find them.
2. Run CMake from `./build` to generate the project files:
	1. `cd build`
	2. `cmake -G "Your Build System" ../`
		* for example, with mingw: `cmake -G "MinGW Makefiles" ../`
3. Compile using your build system
	* i.e. `mingw32-make` 

But why do this with C++ instead of Ruby or Python?
===================================================
1. I wanted to learn more about [Boost](http://www.boost.org)
2. I wanted to learn more about [cmake](http://www.cmake.org) and how to build cross-platform programs with C++ (practice practice practice!)
3. I wanted to see what it would take to get a single binary with no run-time dependencies that could be put on any machine without worrying about whether that machine has Python, or what version of Ruby, or if it has the tar gem installed etc.

So what did I learn?

* It really wasn't that bad to write this in modern C++11 ... more on this in a moment.
* C++ __desperately__ needs a standardized package manager similar to Ruby gem or Python's pip. A C++ "package" should consist of:
	+ headers
	+ static and dynamic libs with 32 and 64 bit versions, with optional debug versions as well all fully built and ready to be linked against-- __and every compiler should be able to use them__. C++ really really needs some kind of standard pre-compiled library format (and there's several [ideas out there](http://clang.llvm.org/docs/Modules.html) on [how to do this](http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2014/n4047.pdf)).
	+ documentation and tutorials on how to use the package
	+ optionally, the source code for the package or a link to it



So... I spent a lot of time trying to figure out how to buil libarchive in such a fashion that I could use it. I eventually read enough documentation and stared at my debugger long enough that I realized I needed zlib installed as well in order for libarchive to use it to create tarballs.

This sort of thing is obvious to anyone who has used libarchive in the past, and the libarchive devs probably think I'm an idiot. But in my defense nobody can be an expert on _everything_. There's too many libraries out there to memorize how to build them, how to link them, what #define macros they require, etc. and all this before you can even start learning how to use the functionality that the library makes available (which is often no small task in itself!).

So as I slogged through figuring out how to get Boost, libarchive, zlib, Visual Studio, and CMake to work together I kept thinking "Why isn't this as easy as it would be on Linux? Where's the package manager?!" It would have made things _so_ much easier to just `sudo apt-get install libarchive zlib boost cmake g++`.

But after getting the libraries to finally link up and getting the code I had written to actually run I finished writing the program in a couple of minutes, then debugged it and tweaked it for a few minutes more. And it was quite fun! C++11 is every bit as easy to use and modern as C# it even looks syntactically very similar. If only C++ had its own package manager or default library like C# does...

Why did the C# people re-invent the wheel instead of helping to improve C++?