#!/usr/bin/ruby

require 'date'
require 'optparse'
require 'pathname'
require 'rbconfig'

#
# make a tarball
# put it in options[:destination]
# check if we have more than maxn backups and delete the oldest if any overage
# print some feedback if options[:verbose] == true
#
def backup options
	
	# windows doesn't support fork so tar cannot use the 'z' option on windows
	case RbConfig::CONFIG['host_os']
		when /mswin|msys|mingw|cygwin|bccwin|wince|emc/
			taropts = 'cf'
			tarext = '.tar'
		when /darwin|mac os|linux|solaris|bsd/
			taropts = 'czf'
			tarext = '.tar.gz'
	end
	timestamp = DateTime.now.strftime '%Y-%m-%d_%I%M%S'
	cleanTargetName = Pathname.new(options[:target]).basename.to_s.downcase.tr(' ', '_')
	targname = "#{cleanTargetName}#{tarext}"
	destination_name = "#{options[:destination]}/#{timestamp}_#{targname}"
	
	tarcmd = "tar #{taropts} #{destination_name} #{options[:target]}"
	if options[:verbose] == true
		puts tarcmd
	end
	`#{tarcmd}`
	
	# delete the oldest if we're over
	# note that because of the way backups are named alphabetical order is the same as oldest first
	backups = Dir[options[:destination] + "/*#{targname}"].sort
	if backups.length > options[:maxn]
		count = 0
		backups[0,backups.length - options[:maxn]].each do |b|
			File.delete b
			count += 1
		end
		if options[:verbose]
			puts "Deleted #{count} files to maintain a maximum of #{options[:maxn]} backups."
		end
	end
end

if __FILE__ == $0
	opts = {
		:destination => './',
		:target => '',
		:maxn => 5,
		:verbose => false
	}
	
	p = OptionParser.new do |o|
		o.on('-d', '--destination DIR', 'Directory to put backups into.') { |d| opts[:destination] = d }
		o.on('-t', '--target PATH', 'File or Dir to backup.') { |t| opts[:target] = t }
		o.on('-m', '--maxn N', 'Maximum number of backups to keep around (oldest will be culled).') { |n| opts[:maxn] = n.to_i }
		o.on('-V', '--verbose', 'Show files and feedback as things run.') { |v| opts[:verbose] = true }
	end
	
	if ARGV.length == 0
		puts p.help
		exit 1
	else
		begin p.parse!
		rescue OptionParser::InvalidOption => e
			puts 'InvalidOption'
			puts p.help
		end
	end
	
	if !File.exists? opts[:destination] or !File.directory? opts[:destination]
		puts "Destination must exist and be a directory: \"#{opts[:destination]}\""
		puts p.help
		exit 1
	end
	if !File.exists? opts[:target]
		puts "Target must exist: \"#{opts[:target]}\""
		puts p.help
		exit 1
	end
	
	backup opts
end