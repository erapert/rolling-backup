backup
======

The backup program makes a rolling a backup of a directory or file. It works by :

* Compress the whole directory or file into a single tarball and put it in some given directory.
* Keep a maximum number of copies, if over then delete the oldest.
* Use time stamps in the file name instead of file system timestamps (because file system time stamps can be thrown off. e.g. Say the file was downloaded over FTP then the file system would have the time when the file was _downloaded_ not when it was _created_ on the server)

File names for backups are in the form:
`YYYY-MM-DD_hhmmss_backup_target_name.tar.gz`

The `test` directory contains files used for testing the implementations to make sure they don't garble the files.
This isn't a problem for the Python and Ruby implementations because they just call the `tar` utility on the command line, but the C++ implementation uses a library and creates the tarballs directly.

Each sub-dir contains an implementation of fetch.
Each works the same way and takes the same command line arguments.

Usage
-----
required options:
<pre>
--destination, -d (path) = a dir to put the backups into
--target, -t (path) = a file or directory to backup
--maxn, -m (int) = maximum number of backups to keep around (oldest will be culled)
</pre>

optional options:
<pre>
--help, -h = print help
--verbose, -V (bool) = print output as it's working
</pre>

complete example:
`rolling_backup.rb --target=./foo/bar/baz --destination=~/backups/go/here --maxn=10 --verbose=1`