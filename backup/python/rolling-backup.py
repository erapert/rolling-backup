#!/usr/bin/python

import argparse, sys, os, platform, glob, time

def backup (opts):
	"""
	make a tarball
	put it in options[:destination]
	check if we have more than maxn backups and delete the oldest if any overage
	print some feedback if options[:verbose] == true
	"""
	# windows doesn't support fork so tar cannot use the 'z' option on windows
	if (platform.system () == 'Windows'):
		taropts = 'cf'
		tarext = '.tar'
	else:
		taropts = 'czf'
		tarext = '.tar.gz'
	timestamp = time.strftime ('%Y-%m-%d_%H%M%S', time.gmtime ())
	cleanTargetName = os.path.basename (opts.targ)
	targname = cleanTargetName + tarext
	destname = opts.dest + '/' + timestamp + '_' + targname
	tarcmd = 'tar ' + taropts + ' ' + destname + ' ' + opts.targ
	
	# make the backup
	if (opts.verb == True):
		print (tarcmd)
	os.system (tarcmd)
	
	# delete the oldest if we're over
	# note that because of the way backups are named alphabetical order is the same as oldest first
	backups = glob.glob (opts.dest + '/*' + targname)
	backups = sorted (backups)
	if (len (backups) > opts.maxn):
		overage = (len (backups) - opts.maxn)
		for i in range (0, overage):
			os.remove (backups [i])
		if (opts.verb == True):
			print ('Deleted ' + str(overage) + ' files to maintain a maximum of ' + str(opts.maxn) + ' backups.')
	

if __name__ == '__main__':
	parser = argparse.ArgumentParser ()
	parser.add_argument ('-d', '--destination',
		dest='dest', required=True, type=str, metavar='DESTINATION_PATH',
		help='Directory to put backups into.')
	parser.add_argument ('-t', '--target',
		dest='targ', required=True, type=str, metavar='TARGET_PATH',
		help='File or Dir to backup.')
	parser.add_argument ('-m', '--maxn',
		dest='maxn', required=True, type=int, metavar='N',
		help='Maximum number of backups to keep around (oldest will be culled).')
	parser.add_argument ('-v', '--verbose',
		dest='verb', required=False, const=True, default=False, type=bool, nargs='?',
		help='Show files and feedback as things run.')
	
	args = parser.parse_args ()
	if (len (sys.argv) == 1):
		parser.print_help ()
	
	backup (args)
	