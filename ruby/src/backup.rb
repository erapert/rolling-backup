module Backup
	require 'logger'
	require_relative './backup_target'
	
	class BackupError < StandardError
	end
	
	class DataError < StandardError
	end
	
	#
	#	feed me a hash and I'll give you an array of Backup::BackupTarget
	#
	def self.target_types
		[ :file, :mysqldump, :sftpfetch, :sftpput, :ftpput ]
	end
	
	class Log
		attr_accessor :stdoutlog, :filelog
		def initialize verbose = false, logfile = nil
			@@timestamp_format = '%Y-%m-%d %I:%M:%S'
			@stdoutlog = (verbose) ? Logger.new(STDOUT) : nil
			@filelog = (logfile) ? Logger.new(logfile, 7, 1024000) : nil
			if @stdoutlog
				@stdoutlog.datetime_format = @@timestamp_format
			end
			if @filelog
				@filelog.datetime_format = @@timestamp_format
			end
		end
		
		def log msg
			@stdoutlog.info(msg) if @stdoutlog
			@filelog.info(msg) if @filelog
		end
		
		def warn msg
			@stdoutlog.warn(msg) if @stdoutlog
			@filelog.warn(msg) if @filelog
		end
		
		def error msg
			@stdoutlog.error(msg) if @stdoutlog
			@filelog.error(msg) if @filelog
		end
	end
end
