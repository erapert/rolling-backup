module Backup
	require 'net/sftp'
	require 'net/ftp'
	require 'date'
	require_relative './backup'
	
	#
	# base class meant to provide the interface for use
	#
	class BackupTarget
		attr_accessor :type, :name, :destination, :target, :maxnumber, :log
		
		def initialize log, target
			@log = log
			
			# init all my required attrs
			required_attrs.each{ |a| a = nil }
			
			set_attrs_from_hash target
			
			# make sure that all required attrs are set
			if !is_valid?
				raise StandardError.new "All attributes of backup targets must be set. Unset attrs: #{invalid_attrs.to_s} for target #{self.to_s}"
			end
			
			# we have to generate the timestamp now, once, so that the timestamp doesn't mis-match later
			# if a process takes more than, say, a second to complete
			@timestamp = DateTime.now.strftime '%Y-%m-%d_%H%M%S'
			
			return self
		end
		
		def is_valid?
			return invalid_attrs.length == 0
		end
		
		def set_attrs_from_hash h
			h.each do |k,v|
				if !self.respond_to?(k)
					raise StandardError.new "Invalid (extra?) attribute: '#{k}' in target #{self.to_s}"
				end
				instance_variable_set("@#{k}", v) if !v.nil?
			end
		end
		
		def invalid_attrs
			rtrn = []
			required_attrs.each do |a|
				if instance_variable_get("@#{a}") == nil
					rtrn << a
				end
			end
			return rtrn
		end
		
		def required_attrs
			return [:type, :name, :destination, :target, :maxnumber]
		end
		
		# spaces -> underscores
		def clean_name
			return @name.downcase.tr(' ', '_')
		end
		
		# returns date + time as "YYYY-MM-DD_hhmmss"
		def timestamp_str
			return @timestamp
		end
		
		def log_and_run cmd
			@log.log "	#{cmd}"
			`#{cmd}`
		end
		
		def trim_files
			if !File.exists?(@destination) or !File.directory?(@destination)
				raise StandardError.new "Path must exist and be a directory: '#{@destination}'"
				return 0
			end
			if !@maxnumber.is_a? Integer or @maxnumber < 1
				raise StandardError.new "#{@name} maxnumber must be a positive integer: #{@maxnumber}"
				return 0
			end
			
			# note that because of the way backups are named alphabetical order is the same as oldest first
			#backups = Dir[File.join(@destination, '*' + clean_name + @target.ext + @target.compressed_ext)].sort
			backups = Dir[File.join(@destination, '*' + clean_name + '*')].sort
			count = 0
			if backups.length > @maxnumber
				backups[0, backups.length - @maxnumber].each do |b|
					File.delete b
					count += 1
				end
			end
			return count
		end
	end
	
	class FileTarget < BackupTarget
		attr_accessor :path, :ext, :compressed_ext
		
		def initialize log, target
			set_attrs_from_hash target[:target]
			super log, target
			@ext = '.tar'
			@compressed_ext = '.gz'
		end
		
		def required_attrs
			return [:path]
		end
		
		def run
			@log.log "Running '#{@name}' ..."
			
			case RbConfig::CONFIG['host_os']
				when /mswin|msys|mingw|cygwin|bccwin|wince|emc/
					# not the final destination name because tar and gzip will add '.tar' and '.gz' to the end
					dest = File.join @destination, timestamp_str + '_' + clean_name + @ext
					
					# gnuwin32 -- tar on windows is completely broken so we'll use bsdtar instead
					log_and_run "bsdtar -cf #{dest} #{@path}"
					
					# -5 = medium compression for speed / size ratio
					# -f = force overwrite of file if it exists
					# (it's assumed we're running unsupervised... so we can't wait around for someone to press 'y')
					log_and_run "gzip -f -5 #{dest}"
				
				# civilized systems (Linux)
				else
					dest = File.join @destination, timestamp_str + '_' + clean_name + @ext + @compressed_ext
					log_and_run "tar -czf #{dest} #{@path}"
			end
			
			num_trimmed = trim_files
			
			@log.log "	Deleted #{num_trimmed} files to maintain a maximum of #{@maxnumber} backups."
			@log.log "Completed '#{@name}'"
			
			return num_trimmed
		end
	end
	
	class MysqldumpTarget < BackupTarget
		attr_accessor :host, :username, :password, :database, :ext, :compressed_ext
		
		def initialize log, target
			set_attrs_from_hash target[:target]
			super log, target
			@ext = '.sql'
			@compressed_ext = '.gz'
		end
		
		def required_attrs
			return [:host, :username, :password, :database]
		end
		
		def run
			@log.log "Running '#{@name}' ..."
			
			pass = (@password and @password != '') ? "-p#{@password}" : ''
			
			dump_destination = File.join @destination, timestamp_str + '_' + clean_name + @ext
			log_and_run "mysqldump -u #{@username} #{pass} -h#{@host} #{@database} > #{dump_destination}"
			
			if File.exists? dump_destination
				log_and_run "gzip #{dump_destination}"
			else
				raise StandardError.new "#{@name}: Failed to create database dump at '#{dump_destination}'"
			end
			
			num_trimmed = trim_files
			
			@log.log "	Deleted #{num_trimmed} files to maintain a maximum of #{@maxnumber} backups."
			@log.log "Completed '#{@name}'"
			
			return num_trimmed
		end
	end
	
	class SftpfetchTarget < BackupTarget
		attr_accessor :host, :username, :password, :path, :ext, :compressed_ext
		
		def initialize log, target
			set_attrs_from_hash target[:target]
			super log, target
			# sftp targets should be left at whatever filename they had on the server
			@ext = ''
			@compressed_ext = ''
		end
		
		def required_attrs
			return [:host, :username, :password, :path]
		end
		
		def run
			@log.log "Running '#{@name}' ..."
			
			Net::SFTP.start(@host, @username, :password => @password) do |sftp|
				@log.log "	Connected to '#{@host}'."
				remote_dir = File.dirname @path
				targ_pattern = File.basename @path
				files = sftp.dir.glob remote_dir, targ_pattern
				
				downloadList = files.sort_by!{|f| f.name.downcase }
				overage = downloadList.length - @maxnumber
				downloadList = downloadList.drop overage if overage > 0
				
				downloadList.each do |f|
					remotePath = "#{remote_dir}/#{f.name}"
					localPath = File.join(@destination, f.name)
					if !File.exists? localPath
						@log.log "	Downloading '#{f.name}'..."
						sftp.download remotePath, localPath
						@log.log '	Done.'
					else
						@log.log "	Skipping download of '#{f.name}' because it already exists locally..."
					end
				end
			end
			
			num_trimmed = trim_files
			
			@log.log "	Deleted #{num_trimmed} files to maintain a maximum of #{@maxnumber} backups."
			@log.log "Completed '#{@name}'"
			
			return num_trimmed
		end
	end
	
	class SftpputTarget < BackupTarget
		attr_accessor :path
		
		def initialize log, target
			set_attrs_from_hash target[:target]
			super log, target
			@ext = ''
			@compressed_ext = ''
		end
		
		def required_attrs
			return [:path]
		end
		
		def run
			@log.log "Running '#{@name}' ..."
			sftp = Net::SFTP.start(@destination[:host], @destination[:username], :password => @destination[:password])
			@log.log "	Connected to '#{@destination[:host]}'."
			
			remote_dir = @destination[:path]
			local_files = Dir.glob(@path)
				.map{ |f| File.basename f }
				.sort_by{ |f| f.downcase }
				.last(@maxnumber)
			
			remote_files = sftp.dir.glob(remote_dir, File.basename(@path))
				.map{ |f| f.name }
				.sort_by{ |f| f.downcase }
			
			# filter so we only transfer files that aren't already present
			files_delta = local_files.reject{ |lf| remote_files.include? lf }
			if files_delta.length > 0
				# send 'em
				files_delta.each do |f|
					p = File.join File.dirname(@path), f
					if !File.exists? p
						raise StandardError.new "	File was supposed to be uploaded but doesn't exist locally?! '#{p}'"
					else
						@log.log "	Uploading '#{p}'..."
						sftp.upload! p, "#{remote_dir}/#{f}"
						@log.log "	Done."
					end
				end
			else
				@log.log "	Nothing to transfer all local files already exist at the destination."
			end
			
			# delete any overage on the remote destination
			rtrn = trim_remote_files sftp
			
			# if the server has it but I don't then show a warning
			# of course we can't upload it, because we don't have it locally
			remote_files_delta = sftp.dir.glob(remote_dir, File.basename(@path))
				.map{ |f| f.name }
				.sort_by{ |f| f.downcase }
				.reject{ |rf| local_files.include? rf }
			
			if remote_files_delta.length > 0
				@log.warn "	Files exist in the remote destination after trimming that don't exist locally:"
				remote_files_delta.each{ |f| @log.warn "		#{f}" }
			end
			
			@log.log "	Deleted #{rtrn} files to maintain a maximum of #{@maxnumber} backups."
			@log.log "Completed '#{@name}'"
			
			return rtrn
		end
		
		def trim_remote_files sftp_connection
			remote_files = sftp_connection.dir.glob(@destination[:path], File.basename(@path))
				.map{ |f| f.name }
				.sort_by{ |f| f.downcase }
			
			count = 0
			if remote_files.length > @maxnumber
				remote_files[0, remote_files.length - @maxnumber].each do |f|
					#@log.log "	deleting '#{@destination[:path]}/#{f}' from remote"
					sftp_connection.remove! "#{@destination[:path]}/#{f}"
					count += 1
				end
			end
			
			return count
		end
	end
	
	class FtpputTarget < BackupTarget
		attr_accessor :path
		
		def initialize log, target
			set_attrs_from_hash target[:target]
			super log, target
			@ext = ''
			@compressed_ext = ''
		end
		
		def required_attrs
			return [:path]
		end
		
		def run
			@log.log "Running '#{@name}' ..."
			ftp = Net::FTP.new @destination[:host]
			if !ftp || ftp == nil
				@log.error "	Failed to connect to '#{@destination[:host]}'"
				return
			end
			@log.log "	Connected to '#{@destination[:host]}'."
			
			if ftp.login @destination[:username], @destination[:password]
				@log.log "	Logged in."
			else
				@log.error "	Failed to log into '#{@destination[:host]}'"
			end
			
			remote_dir = @destination[:path]
			local_files = Dir.glob(@path)
				.map{ |f| File.basename f }
				.sort_by{ |f| f.downcase }
				.last(@target.maxnumber)
			
			remote_files = ftp.dir.glob(remote_dir, File.basename(@path))
				.map{ |f| f.name }
				.sort_by{ |f| f.downcase }
			
			# filter so we only transfer files that aren't already present
			files_delta = local_files.reject{ |lf| remote_files.include? lf }
			if files_delta.length > 0
				# send 'em
				files_delta.each do |f|
					p = File.join File.dirname(@path), f
					if !File.exists? p
						raise StandardError.new "	File was supposed to be uploaded but doesn't exist locally?! '#{p}'"
					else
						@log.log "	Uploading '#{p}'..."
						ftp.putbinaryfile p, "#{remote_dir}/#{f}"
						@log.log "	Done."
					end
				end
			else
				@log.log "	Nothing to transfer all local files already exist at the destination."
			end
			
			# delete any overage on the remote destination
			rtrn = trim_remote_files ftp
			
			# if the server has it but I don't then show a warning
			# of course we can't upload it, because we don't have it locally
			remote_files_delta = ftp.nlst(remote_dir, File.basename(@path))
				.map{ |f| f.name }
				.sort_by{ |f| f.downcase }
				.reject{ |rf| local_files.include? rf }
			
			if remote_files_delta.length > 0
				@log.warn "	Files exist in the remote destination after trimming that don't exist locally:"
				remote_files_delta.each{ |f| @log.warn "		#{f}" }
			end
			
			@log.log "	Deleted #{rtrn} files to maintain a maximum of #{@maxnumber} backups."
			@log.log "Completed '#{@name}'"
			
			return rtrn
		end
		
		def trim_remote_files ftp_connection
			remote_files = ftp_connection.nlst(@destination[:path], File.basename(@path))
				.map{ |f| f.name }
				.sort_by{ |f| f.downcase }
			
			count = 0
			if remote_files.length > @maxnumber
				remote_files[0, remote_files.length - @maxnumber].each do |f|
					#@log.log "	deleting '#{@destination[:path]}/#{f}' from remote"
					ftp_connection.delete "#{@destination[:path]}/#{f}"
					count += 1
				end
			end
			
			return count
		end
	end
end
