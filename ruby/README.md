rolling-backup
==============

Frequently developers need to generate and keep a rolling backup of something. They don't need, or it's politically impossible to obtain, a full-scale "enterprise" class backup solution when a quick little script could do the job.

This program is meant to be run via [cron](https://en.wikipedia.org/wiki/Cron) or [systemd timers](https://wiki.archlinux.org/index.php/Systemd/Timers).

Requirements
------------

`gem install net-sftp`

Usage
-----

Silent: `rolling-backup.rb`
Verbose + Logging to file: `rolling-backup.rb -v -l`
Use a non-default config file: `rolling-backup.rb -c ./foo/bar.cfg`

By default it will use `./rolling-backup.cfg` as the config file.
The log file is `./rolling-backup.log` by default (it will keep up to seven 1MB files and age out the oldest as it goes)

Config file
-----------
The config file is just JSON. It should look like so:

```json
{
	"backup_targets": [
		{
			"type": "file",
			"name": "foo files",
			"destination": "./backups",
			"target": {
				"path": "./test/test_target_dir"
			},
			"maxnumber": 3
		},
		{
			"type": "mysqldump",
			"name": "foo db",
			"destination": "./backups",
			"target": {
				"host": "localhost",
				"username": "foo",
				"password": "asdf",
				"database": "foo_test"
			},
			"maxnumber": 3
		},
		{
			"type": "sftp",
			"name": "foo fetch",
			"destination": "./backups",
			"target": {
				"host": "foo.bar.baz",
				"username": "foo",
				"password": "asdf",
				"path": "/home/foo/backups/*foo.tar.gz"
			},
			"maxnumber": 3
		}
	]
}
```

The program should error out if there are extra or missing fields.