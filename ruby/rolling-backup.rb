require 'optparse'
require 'json'

require_relative './src/backup'

def run cfg
	log = cfg[:logger]
	log.log 'Starting...'
	cfg[:backup_targets].each do |target|
		begin
			b = Backup.const_get(target[:type].capitalize + 'Target')
			if b
				b.new(log, target).run
			else
				raise "No backup type set for #{target}. Valid types: [#{Backup::target_types.join(', ')}]."
			end
		rescue StandardError => e
			log.error e
			log.log 'continuing...'
		end
	end
	log.log 'Rolling backup complete!'
end


if __FILE__ == $0
	cfg = {
		:cfg_fname => 'rolling-backup.cfg',
		:logfile => nil,
		:verbose => false,
		:cfg => {}
	}
	
	p = OptionParser.new do |o|
		o.on('-c', '--config [CONFIGFILE]', 'File containing config in JSON format.') { |c| cfg[:cfg_fname] = c }
		o.on('-v', '--verbose', 'Print output during execution') { cfg[:verbose] = true }
		o.on('-l', '--log [LOGFILE]', 'Log output to the logfile') { |l| cfg[:logfile] = (l) ? l : 'rolling-backup.log' }
	end
	
	begin p.parse!
	rescue OptionParser::InvalidOption => e
		puts "InvalidOption: #{e}"
		puts p.help
		exit 1
	end
	
	cfg[:logger] = Backup::Log.new cfg[:verbose], cfg[:logfile]
	
	# load the config file and validate it
	cfg.merge!(JSON.parse(
		File.read(cfg[:cfg_fname]),
		:symbolize_names => true
	))
	
	run cfg
end
